# Employee API Dev Guide

## Software requirements
- Java 11+
- Docker, docker-compose

## Dependencies
App has single dependency on MySQL server. It can be run via docker-compose. See below

## Building
To build project on local machine run gradle command:

`./gradlew clean build`

Or you can import the project in your favourite IDE and build it there

## Testing
`./gradlew clean test` - will run unit test

Also, I have prepared an Integration test which starts up the application and uses real DB.
To run it execute `./gradlew clean integrationTest` or run `EmployeeIntegrationTest` from IDE

## Developing
First of all, need to start MySQL server. This can be done via command:
`docker-compose -f docker-compose-local.yml up`

To run application you can just run main class `EmployeeApplication` from IDE and choose `dev` Spring profile.
Or alternatively via gradle: `./gradlew bootRunDev`

## Deploying
To deploy app, first need to build docker image:
`./gradlew bootBuildImage --imageName=employee-crud`

Then you can run app via docker-compose:
`docker-compose -f docker-compose-prod.yml up`

For the first time you may need to rerun this command because mysql server doesn't get created after its completion

## REST API

There's an OpenAPI document available at `http://localhost:8080/api-docs`

Also you can access Swagger UI at `http://localhost:8080/swagger-ui.html`
