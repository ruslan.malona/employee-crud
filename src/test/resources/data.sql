INSERT INTO employees(name, position, start_date) VALUES('Jack', 'CEO', now());
INSERT INTO employees(name, position, superior, start_date) VALUES('Ruslan', 'TL', 1, now());
INSERT INTO employees(name, position, superior, end_date) VALUES('Ivan', 'Developer', 2, now());
INSERT INTO employees(name, position, superior, end_date) VALUES('Pavel', 'Developer', 2, now());
