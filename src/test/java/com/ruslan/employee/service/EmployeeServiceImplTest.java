package com.ruslan.employee.service;

import com.ruslan.employee.dto.EmployeeDTO;
import com.ruslan.employee.dto.EmployeeModificationDTO;
import com.ruslan.employee.exception.EmployeeNotFoundException;
import com.ruslan.employee.model.EmployeeEntity;
import com.ruslan.employee.repository.EmployeeRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class EmployeeServiceImplTest {
    @Mock
    private EmployeeRepository employeeRepository;
    @InjectMocks
    private EmployeeServiceImpl employeeService;
    @Captor
    private ArgumentCaptor<EmployeeEntity> entityArgumentCaptor;

    public static final Long ID = 1L;
    public static final Long SUPERIOR_ID = 0L;
    public static final String NAME = "name";
    public static final String POSITION = "position";
    public static final LocalDateTime END_DATE = LocalDateTime.now();
    public static final LocalDateTime START_DATE = END_DATE.minusDays(1L);

    @Test
    public void findAllChildEmployees_shouldSearchMultipleLevels() {
        var firstLevelEmployee = getBareEntity(ID);
        var secondLevelEmployee = getBareEntity(2L);
        var thirdLevelEmployee = getBareEntity(3L);

        when(employeeRepository.findById(ID)).thenReturn(Optional.of(firstLevelEmployee));
        when(employeeRepository.findAllBySuperiorIn(List.of(firstLevelEmployee))).thenReturn(List.of(secondLevelEmployee));
        when(employeeRepository.findAllBySuperiorIn(List.of(secondLevelEmployee))).thenReturn(List.of(thirdLevelEmployee));

        var allChildEmployees = employeeService.findAllChildEmployees(ID);
        assertThat(allChildEmployees).containsExactly(getBareDTO(2L), getBareDTO(3L));
    }

    @Test
    public void findAllChildEmployees_shouldThrowExceptionIfNoEmployee() {
        when(employeeRepository.findById(ID)).thenReturn(Optional.empty());

        assertThrows(EmployeeNotFoundException.class, () ->
                employeeService.findAllChildEmployees(ID));
    }

    @Test
    public void createEmployee_shouldCallSaveWithRightArguments() {
        when(employeeRepository.save(any(EmployeeEntity.class))).thenReturn(getSavedEntity());

        employeeService.createEmployee(getEmployeeModificationDTO());

        verify(employeeRepository).save(entityArgumentCaptor.capture());
        var actualEntity = entityArgumentCaptor.getValue();
        assertThat(actualEntity).isNotNull();
        assertThat(actualEntity.getId()).isNull();
        assertThat(actualEntity.getSuperior()).isNull();
        assertThat(actualEntity.getName()).isEqualTo(NAME);
        assertThat(actualEntity.getPosition()).isEqualTo(POSITION);
        assertThat(actualEntity.getStartDate()).isEqualTo(START_DATE);
        assertThat(actualEntity.getEndDate()).isEqualTo(END_DATE);
    }

    @Test
    public void updateEmployee_shouldCallSaveWithRightArguments() {
        var savedEntity = getSavedEntity();
        when(employeeRepository.findById(ID)).thenReturn(Optional.of(savedEntity));
        when(employeeRepository.save(any(EmployeeEntity.class))).thenReturn(savedEntity);

        var updateDTO = getUpdateDTO();
        employeeService.updateEmployee(ID, updateDTO);

        verify(employeeRepository).save(entityArgumentCaptor.capture());
        var actualEntity = entityArgumentCaptor.getValue();
        assertThat(actualEntity).isNotNull();
        assertThat(actualEntity.getId()).isEqualTo(ID);
        assertThat(actualEntity.getSuperior()).isEqualTo(savedEntity.getSuperior());
        assertThat(actualEntity.getName()).isEqualTo(updateDTO.getName());
        assertThat(actualEntity.getPosition()).isEqualTo(updateDTO.getPosition());
        assertThat(actualEntity.getStartDate()).isEqualTo(updateDTO.getStartDate());
        assertThat(actualEntity.getEndDate()).isEqualTo(updateDTO.getEndDate());
    }

    @Test
    public void updateEmployee_shouldThrowExceptionIfNoEmployee() {
        when(employeeRepository.findById(ID)).thenReturn(Optional.empty());

        assertThrows(EmployeeNotFoundException.class, () ->
                employeeService.updateEmployee(ID, getEmployeeModificationDTO()));
    }

    @Test
    public void getEmployee_shouldReturnCorrectDTO() {
        when(employeeRepository.findById(ID)).thenReturn(Optional.of(getSavedEntity()));

        var employeeDTO = employeeService.getById(ID);
        assertThat(employeeDTO).isEqualTo(getEmployeeDTO());
    }

    @Test
    public void getEmployee_shouldThrowExceptionIfNoEmployee() {
        when(employeeRepository.findById(ID)).thenReturn(Optional.empty());

        assertThrows(EmployeeNotFoundException.class, () ->
                employeeService.getById(ID));
    }

    @Test
    public void findByPosition_shouldReturnJustDelegate() {
        when(employeeRepository.findAllByPosition(POSITION)).thenReturn(List.of(getSavedEntity()));

        var byPosition = employeeService.findByPosition(POSITION);

        assertThat(byPosition).containsExactly(getEmployeeDTO());
    }

    @Test
    public void addSuperiorEmployee_shouldSaveWithRightArguments() {
        var superiorEmployee = new EmployeeEntity();
        var employee = new EmployeeEntity();

        when(employeeRepository.findById(ID)).thenReturn(Optional.of(employee));
        when(employeeRepository.getById(SUPERIOR_ID)).thenReturn(superiorEmployee);

        employeeService.addSuperiorEmployee(ID, SUPERIOR_ID);

        verify(employeeRepository).save(entityArgumentCaptor.capture());
        var actualEntity = entityArgumentCaptor.getValue();
        assertThat(actualEntity.getSuperior()).isEqualTo(superiorEmployee);
    }

    @Test
    public void addSuperiorEmployee_shouldThrowExceptionIfNoEmployee() {
        when(employeeRepository.findById(ID)).thenReturn(Optional.empty());

        assertThrows(EmployeeNotFoundException.class, () ->
                employeeService.addSuperiorEmployee(ID, SUPERIOR_ID));
    }

    @Test
    public void addSuperiorEmployee_shouldThrowExceptionIfNoSuperiorEmployee() {
        var employee = new EmployeeEntity();
        when(employeeRepository.findById(ID)).thenReturn(Optional.of(employee));
        when(employeeRepository.getById(SUPERIOR_ID)).thenThrow(new EntityNotFoundException());

        assertThrows(EmployeeNotFoundException.class, () ->
                employeeService.addSuperiorEmployee(ID, SUPERIOR_ID));
    }

    @Test
    public void deleteEmployee_shouldJustDelegate() {
        var savedEntity = getSavedEntity();
        when(employeeRepository.findById(ID)).thenReturn(Optional.of(savedEntity));

        employeeService.deleteEmployee(ID);

        verify(employeeRepository).delete(savedEntity);
    }

    @Test
    public void deleteEmployee_shouldThrowExceptionIfNoEmployee() {
        when(employeeRepository.findById(ID)).thenReturn(Optional.empty());

        assertThrows(EmployeeNotFoundException.class, () ->
                employeeService.deleteEmployee(ID));
    }

    private EmployeeEntity getSavedEntity() {
        var superiorEntity = new EmployeeEntity();
        superiorEntity.setId(SUPERIOR_ID);

        var entity = new EmployeeEntity();
        entity.setId(1L);
        entity.setName(NAME);
        entity.setPosition(POSITION);
        entity.setStartDate(START_DATE);
        entity.setEndDate(END_DATE);
        entity.setSuperior(superiorEntity);

        return entity;
    }

    private EmployeeDTO getEmployeeDTO() {
        var superiorDTO = new EmployeeDTO(SUPERIOR_ID, null, null, null, null, null);
        return new EmployeeDTO(ID, NAME, POSITION, superiorDTO, START_DATE, END_DATE);
    }

    private EmployeeModificationDTO getEmployeeModificationDTO() {
        return new EmployeeModificationDTO(NAME, POSITION, START_DATE, END_DATE);
    }

    private EmployeeModificationDTO getUpdateDTO() {
        return new EmployeeModificationDTO(
                "new name",
                "new position",
                LocalDateTime.now().minusYears(3),
                LocalDateTime.now()
        );
    }

    private EmployeeEntity getBareEntity(Long id) {
        var entity = new EmployeeEntity();
        entity.setId(id);
        return entity;
    }

    private EmployeeDTO getBareDTO(Long id) {
        var employeeDTO = new EmployeeDTO();
        employeeDTO.setId(id);
        return employeeDTO;
    }
}
