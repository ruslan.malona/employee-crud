package com.ruslan.employee.repository;

import com.ruslan.employee.model.EmployeeEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@TestPropertySource(properties = {
        "spring.jpa.hibernate.ddl-auto=validate"
})
public class EmployeeRepositoryTest {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Test
    public void shouldFindEmployeeByIdWithRelations() {
        Optional<EmployeeEntity> employeeOptional = employeeRepository.findById(2L);
        assertThat(employeeOptional).isNotEmpty();

        EmployeeEntity employeeEntity = employeeOptional.get();

        assertThat(employeeEntity.getName()).isEqualTo("Ruslan");
        assertThat(employeeEntity.getPosition()).isEqualTo("TL");
        assertThat(employeeEntity.getSuperior()).isNotNull();
    }

    @Test
    public void shouldFindEmployeesBySuperior() {
        Optional<EmployeeEntity> employeeOptional = employeeRepository.findById(2L);
        assertThat(employeeOptional).isNotEmpty();

        var allBySuperior = employeeRepository.findAllBySuperiorIn(List.of(employeeOptional.get()));

        assertThat(allBySuperior).hasSize(2);
    }

    @Test
    public void shouldFindEmployeesByPosition() {
        List<EmployeeEntity> developers = employeeRepository.findAllByPosition("Developer");
        assertThat(developers).hasSize(2);
    }
}
