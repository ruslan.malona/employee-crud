package com.ruslan.employee.web;

import com.ruslan.employee.dto.EmployeeDTO;
import com.ruslan.employee.dto.EmployeeModificationDTO;
import com.ruslan.employee.exception.EmployeeNotFoundException;
import com.ruslan.employee.service.EmployeeService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = EmployeeController.class)
@AutoConfigureMockMvc
public class EmployeeControllerTest {
    public static final LocalDateTime DEFAULT_TIME = LocalDateTime.parse("2021-07-07T01:02:03");
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmployeeService employeeService;

    @Test
    public void shouldCorrectlyDeserializeEmployee() throws Exception {
        var ceo = new EmployeeDTO(1L, "Jack", "CEO", null, DEFAULT_TIME, null);
        var pm = new EmployeeDTO(1L, "John", "PM", ceo, DEFAULT_TIME, DEFAULT_TIME);
        when(employeeService.getById(1L)).thenReturn(pm);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/employees/1"))
                .andExpect(status().isOk())
                .andExpect(content().json(loadResource("webresources/employee-response.json")));
    }

    @Test
    public void shouldSerializeCreateEmployeeRequest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/employees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(loadResource("webresources/create-employee-request.json")))
                .andExpect(status().isOk());
        verify(employeeService).createEmployee(new EmployeeModificationDTO("Ruslan", "TL", null, null));
    }

    @Test
    public void shouldReturnNotFoundWhenNoEmployeeById() throws Exception {
        when(employeeService.getById(1L)).thenThrow(new EmployeeNotFoundException("ex"));

        mockMvc.perform(MockMvcRequestBuilders.get("/api/employees/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldReturnNotFoundWhenTryToUpdateUknownEmployee() throws Exception {
        when(employeeService.updateEmployee(eq(1L), any(EmployeeModificationDTO.class))).thenThrow(new EmployeeNotFoundException("ex"));

        mockMvc.perform(MockMvcRequestBuilders.put("/api/employees/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(loadResource("webresources/create-employee-request.json")))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldPerformCreateEmployeeValidation() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/employees")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{}"))
                .andExpect(status().isBadRequest())
                .andExpect(content().json(loadResource("webresources/create-employee-validation-errors.json")));
        verify(employeeService, never()).createEmployee(any(EmployeeModificationDTO.class));
    }

    private String loadResource(String fileName) throws Exception {
        var fileUri = this.getClass().getClassLoader().getResource(fileName).toURI();
        return Files.readString(Paths.get(fileUri));
    }
}
