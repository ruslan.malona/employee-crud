package com.ruslan.employee;

import com.ruslan.employee.dto.EmployeeDTO;
import com.ruslan.employee.dto.EmployeeModificationDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest(classes = TestConfig.class, webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EmployeeIntegrationTest {
    @Autowired
    private TestRestTemplate restTemplate;


    @Test
    public void shouldPerformBasicCrudOperations() {
        // Create
        var createEmployee = new EmployeeModificationDTO("Ruslan", "TL", null, null);
        var response = restTemplate.postForEntity("/api/employees", createEmployee, EmployeeDTO.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        var employeeDTO = response.getBody();

        // Update
        var updateEmployee = new EmployeeModificationDTO(employeeDTO.getName(), employeeDTO.getPosition(), LocalDateTime.now(), null);
        restTemplate.put("/api/employees/" + employeeDTO.getId(), updateEmployee);

        // Read
        response = restTemplate.getForEntity("/api/employees/" + employeeDTO.getId(), EmployeeDTO.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        employeeDTO = response.getBody();
        assertThat(employeeDTO.getStartDate()).isNotNull();

        // Delete
        restTemplate.delete("/api/employees/" + employeeDTO.getId());
        response = restTemplate.getForEntity("/api/employees/" + employeeDTO.getId(), EmployeeDTO.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }
}
