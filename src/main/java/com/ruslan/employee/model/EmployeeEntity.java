package com.ruslan.employee.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "employees")
@Getter
@Setter
public class EmployeeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String position;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "superior")
    private EmployeeEntity superior;

    private LocalDateTime startDate;

    private LocalDateTime endDate;
}
