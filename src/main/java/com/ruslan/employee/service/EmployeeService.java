package com.ruslan.employee.service;

import com.ruslan.employee.dto.EmployeeDTO;
import com.ruslan.employee.dto.EmployeeModificationDTO;

import java.util.List;

public interface EmployeeService {
    EmployeeDTO createEmployee(EmployeeModificationDTO employeeDTO);

    EmployeeDTO updateEmployee(Long employeeId, EmployeeModificationDTO employeeDTO);

    EmployeeDTO getById(Long employeeId);

    List<EmployeeDTO> findAllChildEmployees(Long employeeId);

    List<EmployeeDTO> findByPosition(String position);

    EmployeeDTO addSuperiorEmployee(Long employeeId, Long superiorEmployeeId);

    void deleteEmployee(Long employeeId);
}
