package com.ruslan.employee.service;

import com.ruslan.employee.dto.EmployeeDTO;
import com.ruslan.employee.dto.EmployeeModificationDTO;
import com.ruslan.employee.exception.EmployeeNotFoundException;
import com.ruslan.employee.model.EmployeeEntity;
import com.ruslan.employee.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    @Override
    public EmployeeDTO createEmployee(EmployeeModificationDTO employeeDTO) {
        var entity = fromDTO(employeeDTO);
        entity = employeeRepository.save(entity);
        return toDTO(entity);
    }

    @Override
    public EmployeeDTO updateEmployee(Long employeeId, EmployeeModificationDTO employeeDTO) {
        var entity = getEmployeeEntity(employeeId);
        updateEntity(entity, employeeDTO);
        employeeRepository.save(entity);
        return toDTO(entity);
    }

    @Override
    public EmployeeDTO getById(Long employeeId) {
        var entity = getEmployeeEntity(employeeId);
        return toDTO(entity);
    }

    @Override
    public List<EmployeeDTO> findAllChildEmployees(Long employeeId) {
        var rootEntity = getEmployeeEntity(employeeId);
        List<EmployeeDTO> childEmployees = new ArrayList<>();
        Queue<EmployeeEntity> entitiesToProcess = new LinkedList<>();
        entitiesToProcess.offer(rootEntity);

        while (!entitiesToProcess.isEmpty()) {
            int currentLevelSize = entitiesToProcess.size();
            List<EmployeeEntity> currentLevelSuperiors = new ArrayList<>(currentLevelSize);
            for (int i = 0; i < currentLevelSize; i++) {
                currentLevelSuperiors.add(entitiesToProcess.poll());
            }
            var childEmployeesOfCurrentLevel = employeeRepository.findAllBySuperiorIn(currentLevelSuperiors);
            for (EmployeeEntity entity : childEmployeesOfCurrentLevel) {
                entitiesToProcess.offer(entity);
                childEmployees.add(toDTO(entity));
            }
        }

        return childEmployees;
    }

    @Override
    public List<EmployeeDTO> findByPosition(String position) {
        return employeeRepository.findAllByPosition(position).stream()
                .map(this::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    public EmployeeDTO addSuperiorEmployee(Long employeeId, Long superiorEmployeeId) {
        try {
            var employeeEntity = getEmployeeEntity(employeeId);
            var superiorRef = employeeRepository.getById(superiorEmployeeId);
            employeeEntity.setSuperior(superiorRef);
            employeeRepository.save(employeeEntity);
            return toDTO(employeeEntity);
        } catch (EntityNotFoundException exception) {
            throw getEmployeeNotFoundExceptionSupplier(superiorEmployeeId).get();
        }
    }

    @Override
    public void deleteEmployee(Long employeeId) {
        var entity = getEmployeeEntity(employeeId);
        employeeRepository.delete(entity);
    }

    private EmployeeEntity fromDTO(EmployeeModificationDTO dto) {
        EmployeeEntity entity = new EmployeeEntity();
        entity.setName(dto.getName());
        entity.setPosition(dto.getPosition());
        entity.setStartDate(dto.getStartDate());
        entity.setEndDate(dto.getEndDate());
        return entity;
    }

    private EmployeeDTO toDTO(EmployeeEntity entity) {
        return new EmployeeDTO(
                entity.getId(),
                entity.getName(),
                entity.getPosition(),
                entity.getSuperior() != null ? toDTO(entity.getSuperior()) : null,
                entity.getStartDate(),
                entity.getEndDate()
        );
    }

    private void updateEntity(EmployeeEntity entity, EmployeeModificationDTO dto) {
        entity.setName(dto.getName());
        entity.setPosition(dto.getPosition());
        entity.setStartDate(dto.getStartDate());
        entity.setEndDate(dto.getEndDate());
    }

    private EmployeeEntity getEmployeeEntity(Long employeeId) {
        return employeeRepository.findById(employeeId)
                .orElseThrow(getEmployeeNotFoundExceptionSupplier(employeeId));
    }
    private Supplier<EmployeeNotFoundException> getEmployeeNotFoundExceptionSupplier(Long employeeId) {
        return () -> new EmployeeNotFoundException("Can't find employee by id=" + employeeId);
    }
}
