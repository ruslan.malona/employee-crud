package com.ruslan.employee.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeDTO {
    Long id;

    String name;

    String position;

    EmployeeDTO superior;

    LocalDateTime startDate;

    LocalDateTime endDate;
}
