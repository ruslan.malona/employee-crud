package com.ruslan.employee.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeModificationDTO {
    @NotEmpty
    String name;

    @NotEmpty
    String position;

    LocalDateTime startDate;

    LocalDateTime endDate;
}
