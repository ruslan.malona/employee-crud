package com.ruslan.employee.repository;

import com.ruslan.employee.model.EmployeeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<EmployeeEntity, Long> {
    List<EmployeeEntity> findAllByPosition(String position);
    List<EmployeeEntity> findAllBySuperiorIn(List<EmployeeEntity> superiors);
}
