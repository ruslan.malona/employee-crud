package com.ruslan.employee.web;

import com.ruslan.employee.dto.EmployeeModificationDTO;
import com.ruslan.employee.dto.EmployeeDTO;
import com.ruslan.employee.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/employees")
@RequiredArgsConstructor
public class EmployeeController {

    private final EmployeeService employeeService;

    @PostMapping
    public ResponseEntity<EmployeeDTO> createEmployee(@RequestBody @Valid EmployeeModificationDTO employeeDTO) {
        return ResponseEntity.ok(employeeService.createEmployee(employeeDTO));
    }

    @GetMapping("{employeeId}")
    public ResponseEntity<EmployeeDTO> getEmployee(@PathVariable Long employeeId) {
        return ResponseEntity.ok(employeeService.getById(employeeId));
    }

    @PutMapping("{employeeId}")
    public ResponseEntity<EmployeeDTO> updateEmployee(@PathVariable Long employeeId,
                                                      @RequestBody @Valid EmployeeModificationDTO employeeDTO) {
        return ResponseEntity.ok(employeeService.updateEmployee(employeeId, employeeDTO));
    }

    @DeleteMapping("{employeeId}")
    public ResponseEntity<?> deleteEmployee(@PathVariable Long employeeId) {
        employeeService.deleteEmployee(employeeId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping
    public ResponseEntity<List<EmployeeDTO>> findEmployees(@RequestParam String position) {
        return ResponseEntity.ok(employeeService.findByPosition(position));
    }

    @GetMapping("{employeeId}/child-employees")
    public ResponseEntity<List<EmployeeDTO>> getChildEmployees(@PathVariable Long employeeId) {
        return ResponseEntity.ok(employeeService.findAllChildEmployees(employeeId));
    }

    @PutMapping("{employeeId}/superior-employee/{superiorEmployeeId}")
    public ResponseEntity<EmployeeDTO> getChildEmployees(@PathVariable Long employeeId, @PathVariable Long superiorEmployeeId) {
        return ResponseEntity.ok(employeeService.addSuperiorEmployee(employeeId, superiorEmployeeId));
    }
}
