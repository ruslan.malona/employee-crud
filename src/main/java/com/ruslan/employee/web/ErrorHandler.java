package com.ruslan.employee.web;

import com.ruslan.employee.exception.EmployeeNotFoundException;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

import static java.util.stream.Collectors.toList;

@ControllerAdvice
public class ErrorHandler {
    @ExceptionHandler(BindException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<List<Error>> handleSpringValidationException(BindException exception) {
        var errors = exception.getAllErrors()
                .stream()
                .map(e -> new Error(getMessage(e)))
                .collect(toList());
        return ResponseEntity.badRequest().body(errors);
    }

    @ExceptionHandler(EmployeeNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<Error> handleNotFoundException(EmployeeNotFoundException exception) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Error(exception.getMessage()));
    }

    private String getMessage(ObjectError objectError) {
        if (objectError instanceof FieldError) {
            FieldError fieldError = (FieldError) objectError;
            return fieldError.getField() + ": " + fieldError.getDefaultMessage() + ", rejectedValue: " + fieldError.getRejectedValue();
        } else {
            return objectError.getObjectName() + ": " + objectError.getDefaultMessage();
        }
    }

    @Data
    private static class Error {
        private final String message;
    }

}
