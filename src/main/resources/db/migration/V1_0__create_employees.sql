CREATE TABLE IF NOT EXISTS `employees`
(
    `id`          bigint       NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name`        varchar(50)  NOT NULL,
    `position`    varchar(50)  NOT NULL,
    `superior`    bigint,
    `start_date`  timestamp,
    `end_date`    timestamp,
    FOREIGN KEY (superior) REFERENCES employees(id)
);
