# Assignment

## Description

Create a small Java service that exposes a RESTful API over CRUD operations on a model.

## Requirements

Given the `employee` model, containing:

`name`

`position`

`superior` the link to another employee with a management position

`startDate`

`endDate`

#### We need to be able to:

1. Save, update, delete and read an employee
2. Find all the child employees of a parent employee with a management position
3. Find all employees, filtered by a specific position
4. Make sure that the above requirements are tested properly both with negative and positive scenarios

## Evaluation

#### We look for:

1. The way you structure your code and the project
2. The code quality (code standards, best practices, design patterns, etc)
3. The way you write tests
4. The way you write docs
5. Attention to details

## Suggestions

Please don't reinvent the wheel. The internet has everything you need. We care about how you solve puzzles and how you
use the legos to build the end product.

You can use whichever 3rd party libraries or frameworks you see fit to get the job done.

## Going in for the kill

If you really want to showoff your skills and impress us, you can do so by using any of the following:

1. https://micronaut.io as the proper alternative to Spring for building microservices
2. https://www.testcontainers.org as the only way to creating fully integrated tests
3. https://gradle.org as the alternative to apache maven
4. HTTP/2 by default

## Deadline

It has to be submitted within 24hours since the moment you received this assignment.

Don't worry about the tight deadline, this assignment is a best effort approach, not a "get all things done no matter
what".

## Deliverable

Create a public personal github repo where you commit and push this assignment, and provide the link to it.
